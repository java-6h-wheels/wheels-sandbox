let http = require('http');
let rest = require('restler');
let configuration = require('../../config/backend-config');

exports.getCitiesList = function (request, response) {

  rest.get(configuration.perEnvironment.backendUrl + '/mongo_cities/all').on('success', (backendResponseBody, backendResponseMeta) => {
    response.json(backendResponseBody);
  });

};


exports.addCity = function (request, response) {
  let id = request.params.id;

  rest.postJson(configuration.perEnvironment.backendUrl + '/mongo_cities/add' + id)
    .on('success', (backendResponseBody, backendResponseMeta) => {
      response.json(backendResponseBody);
    })
    .on('fail', (backendResponseBody, backendResponseMeta) => {
      response.status(backendResponseMeta.statusCode).send(backendResponseBody);
    });
};
