import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule} from "@angular/forms";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import { HomePageComponent } from './components/home-page/home-page.component';
import { FilterPageComponent } from './components/filter-page/filter-page.component';
import { CarPageComponent } from './components/car-page/car-page.component';
import {UrlInterceptor} from "./interceptors/url.interceptor";
import {ErrorInterceptor} from "./interceptors/error.interceptor";
import {CarDetailsComponent} from "./components/car-details/car-details.component";
import {MainPageComponent} from "./main-page/main-page.component";

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    FilterPageComponent,
    CarPageComponent,
    CarDetailsComponent,
    MainPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    //{provide: HTTP_INTERCEPTORS, useClass: UrlInterceptor, multi: true},
    //{provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
