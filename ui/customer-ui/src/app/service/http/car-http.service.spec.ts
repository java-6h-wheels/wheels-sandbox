import { TestBed } from '@angular/core/testing';

import { CarMockService } from './car-mock.service';

describe('CarHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CarMockService = TestBed.get(CarMockService);
    expect(service).toBeTruthy();
  });
});
