import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Filter} from "../../model/Filter";
import {CarPreview} from "../../model/CarPreview";
import {BehaviorSubject, Observable} from "rxjs";
import {Car} from "../../model/Car";
import {City} from "../../model/City";
import {Category} from "../../model/Category";
import {RentDate} from "../../model/RentDate";
import {Comment} from "../../model/Comment";

@Injectable({
  providedIn: 'root'
})
export class CarService {

  cars:CarPreview[] = [];
  httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
    //'Access-Control-Allow-Origin': '*'
    // 'Authorization': this.jwt
  })};

  carsSubject: BehaviorSubject<any> = new BehaviorSubject<any>({});
  selectedCarSubject: BehaviorSubject<any> = new BehaviorSubject<any>({});

  constructor(private httpClient: HttpClient) {
  }

  getCarsByFilter(filter: Filter){
    return this.httpClient.post<CarPreview[]>("/user/get_cars_by_filter", filter, this.httpOptions);
  }
  getAllCars(){
    return this.httpClient.get<Car[]>("/admin/get_all_cars");
  }
  getAllCarsPreview(){
    return this.httpClient.get<CarPreview[]>("/admin/get_all_car_preview");
  }
  getAllCategories(){
    return this.httpClient.get<Category[]>("/user/get_all_categories");
  }
  getCarByNumber(carNumber: string){
    return this.httpClient.get<Car>("/user/renter/get_car_by_number?carNumber="+carNumber);
  }
  getRentDatesByCar(carNumber: string){
    return this.httpClient.get<RentDate[]>("/admin/get_rentDate_by_car?carNumber="+carNumber);
  }
  addCar(car: Car){
    return this.httpClient.post<Car>("/user/supplier/add_car", car, this.httpOptions);
  }
  addCity(city: City){
    return this.httpClient.post<City>("/admin/add_city", city, this.httpOptions);
  }
  addCategory(category: Category){
    return this.httpClient.post("/admin/add_category", category, this.httpOptions);
  }
  addRentDate(rentDate:RentDate){
    return this.httpClient.post<RentDate>("/user/supplier/add_rental_date",rentDate, this.httpOptions);
  }
  sendComment(comment:Comment){
    return this.httpClient.post<Comment>("/user/renter/send_comment", comment, this.httpOptions);
  }
}
