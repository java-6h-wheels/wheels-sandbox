import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {BehaviorSubject} from "rxjs";
import {User} from "../../model/User";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      // 'Authorization': this.jwt
    })};

  usersSubject: BehaviorSubject<any> = new BehaviorSubject<any>({});

  constructor(private httpClient: HttpClient) {
  }
  getAllUsers(){
    return this.httpClient.get("/admin/user/get_all_users");
  }
  getUserByEmail(email:string){
    return this.httpClient.get("/user/get_user_by_email?email="+email);
  }
  addUser(user: User){
    return this.httpClient.post<User>("/admin/user/create_user", user, this.httpOptions);
  }

}
