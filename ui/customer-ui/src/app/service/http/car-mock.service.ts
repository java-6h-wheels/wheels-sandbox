import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject} from "rxjs";
import {Car} from "../../model/car";

@Injectable({
  providedIn: 'root'
})
export class CarMockService {



  constructor(private httpClient: HttpClient) {
  }

  getCar() {
    return this.httpClient.get("/api/car")
  }

}

