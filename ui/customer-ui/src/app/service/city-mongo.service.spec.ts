import { TestBed } from '@angular/core/testing';

import { CityMongoService } from './city-mongo.service';

describe('MongoCityServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CityMongoService = TestBed.get(CityMongoService);
    expect(service).toBeTruthy();
  });
});
