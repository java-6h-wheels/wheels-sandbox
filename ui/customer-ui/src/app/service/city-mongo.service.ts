import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {MongoCity} from "../model/MongoCity";



@Injectable({
  providedIn: 'root'
})
export class CityMongoService {

  citiesSubject: BehaviorSubject<MongoCity[]> = new BehaviorSubject<MongoCity[]>([]);

  constructor(private httpClient: HttpClient) { }

  getCities(){
    return this.httpClient.get<MongoCity[]>("/mongo_cities/all");
  }

  addCity(cityName: string){
    return this.httpClient.post<MongoCity>("/mongo_cities/add?cityName=" + cityName, '');
  }
}
