import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {City} from "../model/City";
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CityService {

  citiesSubject: BehaviorSubject<any> = new BehaviorSubject<any>({});
  constructor(private httpClient: HttpClient) { }

  getCities(){
    return this.httpClient.get<City[]>("/user/get_all_cities")
  }
}
