import {Component, OnInit, ViewChild} from '@angular/core';
import {CityService} from "../../service/city.service";
import {City} from "../../model/City";
import {CarService} from "../../service/http/car.service";
import {CarPreview} from "../../model/CarPreview";
import {Filter} from "../../model/Filter";
import {Car} from "../../model/Car";
import {RentDate} from "../../model/RentDate";
import {Comment} from "../../model/Comment";
import {Router} from "@angular/router";
import {CityMongoService} from "../../service/city-mongo.service";
import {MongoCity} from "../../model/MongoCity";

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  // @ts-ignore
  @ViewChild('sel') sel;
  // @ts-ignore
  @ViewChild('sel1') sel1;

  cities: MongoCity[] = [];
  city:City = {name:""};
  inputCity:string;

  selectedCity = "";
  selectedCar = "";
  startDateRent = "";
  endDateRent = "";
  cars: CarPreview[] = []
  carsNumber: String[] = [];
  car: Car;
  rentDates: RentDate[] = [];
  rentDate:RentDate = {carNumber:"", startRent:"", endRent:""};
  comment:Comment = {carNumber:"", comment:""};
  filter:Filter = {carNumber: "",
    category:"",
    model: "",
    startYearIssue: 0,
    endYearIssue: 0,
    engine: 0,
    color: "",
    ac: true,
    autoTransmission: true,
    city: "",
    startRentPrice: 0,
    endRentPrice: 0,
    startRent: "",
    endRent: ""};

  constructor(private router: Router, private cityService: CityService, private carService: CarService, private mongoCityService: CityMongoService) { }

  ngOnInit() {
    this.mongoCityService.getCities().subscribe((res:any) => this.cities = res);
    this.mongoCityService.citiesSubject.next(this.cities);
//    this.cityService.getCities().subscribe((res:any) => this.cities = res);
//    this.cityService.citiesSubject.next(this.cities);
    this.carService.getAllCarsPreview().subscribe((res:any)=>this.carsNumber = res.map(car=>car.carNumber));
  }

  goTo(arg){
    this.carService
      .getCarsByFilter(this.filter)
      .subscribe((res:any) => this.carService.carsSubject.next(res));
    this.router.navigate([arg]);
  }
  goToPage(page){
    this.router.navigate([page]);
  }
  getSelectedCity(){
    Array.prototype.filter.call(this.sel.nativeElement.options, opt=>opt.selected)
      .map(opt=>{this.filter.city = opt.value});
  }
  getSelectedCar(){
    Array.prototype.filter.call(this.sel1.nativeElement.options, opt=>opt.selected)
      .map(opt=>{this.selectedCar = opt.value});
  }
  getCarByNumber(){
    this.carService.getCarByNumber(this.selectedCar).subscribe((res:any)=>this.car = res);
  }
  getRentDatesByCar(){
    this.carService.getRentDatesByCar(this.selectedCar).subscribe((res:any)=>this.rentDates = res);
  }
  addRentDates(){
    this.rentDate.carNumber = this.selectedCar;
    this.rentDate.startRent = this.startDateRent;
    this.rentDate.endRent = this.endDateRent;
    this.carService.addRentDate(this.rentDate).subscribe((res: any)=>{
      console.log(res);
    });
  }
  onKey(event){
    this.inputCity = event.target.value;
  }
  addCity(){
    this.city.name = this.inputCity;
    this.mongoCityService.addCity(this.city.name).subscribe((res:any)=>this.cities.push(res));
    this.inputCity = "";
  }

  findCars(){
    this.carService
      .getCarsByFilter(this.filter)
      .subscribe((res:any) => this.cars = res);
    this.carService.carsSubject.next(this.cars);
    console.log(this.carService.carsSubject.getValue());
  }
  sendComment(){
    this.comment.carNumber = this.selectedCar;
    this.comment.comment = "Hello";
    this.carService.sendComment(this.comment).subscribe(res=>console.log(res));
  }


}
