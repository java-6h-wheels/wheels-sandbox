import {Component, Input, OnInit} from '@angular/core';
import carMock from "../../../mock/getCarById.json";



@Component({
  selector: 'app-car-details',
  templateUrl: './car-details.component.html',
  styleUrls: ['./car-details.component.css']
})
export class CarDetailsComponent implements OnInit {


  public car;

  constructor() {

  }

  getCar () {
    return this.car=carMock;
  }

  ngOnInit() {
    this.getCar();
  }

}
