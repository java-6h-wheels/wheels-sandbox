import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-car-page',
  templateUrl: './car-page.component.html',
  styleUrls: ['./car-page.component.css']
})
export class CarPageComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goTo(arg){
    this.router.navigate([arg]);
  }

}
