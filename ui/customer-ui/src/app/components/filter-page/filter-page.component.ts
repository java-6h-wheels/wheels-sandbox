import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import {CarService} from "../../service/http/car.service";
import {CarPreview} from "../../model/CarPreview";
import {City} from "../../model/City";
import {CityService} from "../../service/city.service";
import {Category} from "../../model/Category";
import {Filter} from "../../model/Filter";
import {log} from "util";

@Component({
  selector: 'app-filter-page',
  templateUrl: './filter-page.component.html',
  styleUrls: ['./filter-page.component.css']
})
export class FilterPageComponent implements OnInit {

  // @ts-ignore
  @ViewChild('selCategory') selCategory;
  // @ts-ignore
  @ViewChild('selCity') selCity;

  cars:CarPreview[] = [];
  cities:City[] = [];
  selectedCity = '';
  categories:Category[] = [];
  selectedCategory = '';
  startRentPrice:number;
  endRentPrice:number;
  startDateRent = '';
  endDateRent = '';
  filter:Filter = {carNumber: "",
    category:"",
    model: "",
    startYearIssue: 0,
    endYearIssue: 0,
    engine: 0,
    color: "",
    ac: true,
    autoTransmission: true,
    city: "",
    startRentPrice: 0,
    endRentPrice: 0,
    startRent: "",
    endRent: ""};

  constructor(private router: Router, private carService: CarService, private cityService:CityService) { }

  ngOnInit() {
    this.carService.carsSubject.subscribe(res => this.cars = res);
    this.cityService.getCities().subscribe(res=>this.cities=res);
    this.carService.getAllCategories().subscribe(res=>this.categories=res);
  }
  getSelectedCity(){
    Array.prototype.filter.call(this.selCity.nativeElement.options, opt=>opt.selected)
      .map(opt=>{this.filter.city = opt.value});
  }
  getSelectedCategory(){
    Array.prototype.filter.call(this.selCategory.nativeElement.options, opt=>opt.selected)
      .map(opt=>{this.filter.category = opt.value});
  }
  getStartPrice(event){
    this.filter.startRentPrice = event.target.value;
  }
  getEndPrice(event){
    this.filter.endRentPrice = event.target.value;
  }
  findCarsByFilter(){
    this.carService.getCarsByFilter(this.filter).subscribe(res=>this.cars=res);
    this.carService.carsSubject.next(this.cars);
    console.log(this.carService.carsSubject.getValue());
  }
  goTo(arg, car:CarPreview){
    this.carService.getCarByNumber(car.carNumber)
      .subscribe((res:any)=>this.carService.selectedCarSubject.next(res));
    this.router.navigate([arg]);
  }
}
