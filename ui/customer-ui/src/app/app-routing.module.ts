import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomePageComponent} from "./components/home-page/home-page.component";
import {FilterPageComponent} from "./components/filter-page/filter-page.component";
import {CarPageComponent} from "./components/car-page/car-page.component";
import {CarDetailsComponent} from "./components/car-details/car-details.component";
import {AppComponent} from "./app.component";

const routes: Routes = [
  {path: 'home-page', component: HomePageComponent},
  {path: 'filter-page', component: FilterPageComponent},
  {path: 'car-page', component: CarPageComponent},
  {path: 'car-details', component: CarDetailsComponent},
  {path: '*', component: HomePageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
