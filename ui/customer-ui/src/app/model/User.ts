export interface User{
  email: string;
  password: string;

  firstName: string;
  lastName: string;
  phoneNumber: string;

  notes: string;
  avatar: string;
}
