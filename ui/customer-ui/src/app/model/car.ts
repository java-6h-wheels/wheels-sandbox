export interface Car{
  carNumber: string;
  ownerEmail: string;
  category: string;
  model: string;
  yearIssue: number;
  engine: number;
  color: string;
  ac: boolean;
  autoTransmission: boolean;
  rentPrice: number;
  notes: string;
  photoUrl: any;
  comments: any;

  city: string;
  street: string;
  house: string;
}
