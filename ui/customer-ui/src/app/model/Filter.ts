export interface Filter{
  carNumber: string;
  category: string;
  model: string;
  startYearIssue: number;
  endYearIssue: number;
  engine: number;
  color: string;
  ac: boolean;
  autoTransmission: boolean;
  city: string;
  startRentPrice: number;
  endRentPrice: number;
  startRent: string;
  endRent: string;
}
