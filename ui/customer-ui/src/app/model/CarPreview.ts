export class CarPreview{
  private _carNumber: string;
  private _photoUrlPreview: string;
  private _model: string;
  private _rentPrice: number;
  private _yearIssue: number;
  private _city: string;

  constructor(carNumber: string, photoUrlPreview: string, model: string, rentPrice: number, yearIssue: number, city: string) {
    this._carNumber = carNumber;
    this._photoUrlPreview = photoUrlPreview;
    this._model = model;
    this._rentPrice = rentPrice;
    this._yearIssue = yearIssue;
    this._city = city;
  }

  get carNumber(): string {
    return this._carNumber;
  }

  get photoUrlPreview(): string {
    return this._photoUrlPreview;
  }

  get model(): string {
    return this._model;
  }

  get rentPrice(): number {
    return this._rentPrice;
  }

  get yearIssue(): number {
    return this._yearIssue;
  }

  get city(): string {
    return this._city;
  }
}
