export interface RentDate{
  carNumber: string;
  startRent: string;
  endRent: string;
}
