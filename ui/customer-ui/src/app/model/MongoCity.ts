export {MongoCity}
 class MongoCity {
  private _name: String;

  constructor(name: String) {
    this._name = name;
  }

  get name(): String {
    return this._name;
  }

  set name(value: String) {
    this._name = value;
  }

}

