package com.wheels.model.web;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wheels.model.entity.Category;
import com.wheels.model.entity.Location;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString

public class Filter {

    private String carNumber;
    private String category;
    private String model;
    private int startYearIssue;
    private int endYearIssue;
    private double engine;
    private String color;
    private boolean ac;
    private boolean autoTransmission;
    private String city;
    private double startRentPrice;
    private double endRentPrice;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private String startRent;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private String endRent;


}
