package com.wheels.model.web;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CarPreviewDto {

    private String carNumber;
    private String photoUrlPreview;
    private String model;
    private double rentPrice;
    private int yearIssue;
    private String city;
}
