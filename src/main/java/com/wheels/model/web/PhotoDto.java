package com.wheels.model.web;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PhotoDto {

    private String photoUrl;
    private String photoUrlPreview;
    private String carNumber;
}
