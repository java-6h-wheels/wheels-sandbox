package com.wheels.model.web;

import com.wheels.model.entity.Location;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CarDto {

    @Size(min = 3, max = 10, message = "The car number not valid")
    private String carNumber;
    private String ownerEmail;
    private String category;
    @Size(min = 1, max = 20, message = "The Model not valid")
    private String model;
    @Positive(message = "Year must be positive or 0")
    private int yearIssue;
    @Positive(message = "Engine must be positive or 0")
    private double engine;
    private String color;
    private boolean ac;
    private boolean autoTransmission;
    @Positive(message = "Price must be positive or 0")
    private double rentPrice;
    private String notes;
    private String[] photoUrl;
    private String[] comments;

    private String city;
    private String street;
    private String house;
}
