package com.wheels.model.web;

import com.wheels.model.entity.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class DtoService {
    @Bean
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }
    @Autowired
    DateTimeFormatter dateTimeFormatter;

    public Function<Car, CarDto> carDto = this::getCarDto;
    public Function<Car, CarPreviewDto> carPreviewDto = this::getCarPreviewDto;
    public Function<City, CityDto> cityDto = this::getCityDto;
    public Function<CityMongo, CityDto> cityDtoMongo = this::getCityDtoMongo;
    public Function<Location, LocationDto> locationDto = this::getLocationDto;
    public Function<Photo, PhotoDto> photoDto = this::getPhotoDto;
    public Function<Category, CategoryDto> categoryDto = this::getCategoryDto;
    public Function<User, UserDto> userDto = this::getUserDto;
    public Function<RentDate, RentDateDto> rentDateDto = this::getRentDateDto;

    public <T, R> List<R> collectionToList(Collection<T> collection, Function<T,R> mapper){
        return collection.stream().map(mapper).collect(Collectors.toList());
    }
    public UserDto getUserDto(User user) {
        return /*modelMapper().map(user, UserDto.class);*/new UserDto(user.getEmail(), user.getPassword(), user.getFirstName(), user.getLastName(), user.getPhoneNumber(),
                user.getNotes(), user.getAvatar());
    }
    public CarDto getCarDto(Car car){
        String[] photos = new String[car.getPhotos().size()];
        String[] comments = new String[car.getComments().size()];
        int i = 0, j = 0;
        for (Photo photo : car.getPhotos())
            photos[i++] = photo.getPhotoUrl();

        for (Comment comment: car.getComments())
            comments[j++] = comment.getComment();
        return new CarDto(car.getCarNumber(),
                car.getOwner().getEmail(),
                car.getCategory().getCategoryName(),
                car.getModel(),
                car.getYearIssue(),
                car.getEngine(),
                car.getColor(),
                car.isAc(),
                car.isAutoTransmission(),
                car.getRentPrice(),
                car.getNotes(),
                photos,
                comments,
                car.getLocation().getCity().getCityName(),
                car.getLocation().getStreet(),
                car.getLocation().getHouse());
    }
    public CarPreviewDto getCarPreviewDto(Car car){
        String photoPreview;
        if (car.getPhotos().size() == 0) photoPreview = "";
        else photoPreview = car.getPhotos().stream()
                .filter(Photo::isMainPhoto).map(Photo::getPhotoUrlPreview).toString();

        return new CarPreviewDto(
                car.getCarNumber(),
                photoPreview,
                car.getModel(),
                car.getRentPrice(),
                car.getYearIssue(),
                car.getLocation().getCity().getCityName());
    }
    public CityDto getCityDto(City city){
        return new CityDto(city.getCityName());
    }

    public LocationDto getLocationDto(Location location){
        return new LocationDto(location.getCar().getCarNumber(), location.getCity().getCityName(),location.getStreet(), location.getHouse());
    }

    public PhotoDto getPhotoDto(Photo photo){
        return new PhotoDto(photo.getPhotoUrl(), photo.getPhotoUrlPreview(), photo.getCar().getCarNumber());
    }
    public CategoryDto getCategoryDto(Category category){
        return new CategoryDto(category.getCategoryName());
    }
    public RentDateDto getRentDateDto(RentDate rentDate){
        return new RentDateDto(rentDate.getCar().getCarNumber(), formatLocalDate(rentDate.getStartRent()), formatLocalDate(rentDate.getEndRent()));
    }
    private String formatLocalDate(LocalDate localDate){
        return localDate == null ? null : localDate.format(dateTimeFormatter);
    }

        //MONGO DB
    public CityDto getCityDtoMongo(CityMongo city){
        return new CityDto(city.getName());
    }

}
