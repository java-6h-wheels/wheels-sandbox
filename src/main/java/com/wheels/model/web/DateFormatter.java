package com.wheels.model.web;

import com.wheels.exceptions.WrongDateTimePatternException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.format.DateTimeFormatter;

@Configuration
public class DateFormatter {

    @Value("${wheels.dateFormat}")
    private String pattern;

    @Bean
    public DateTimeFormatter getDateTimeFormatter(){
        try {
            return DateTimeFormatter.ofPattern(pattern);
        }catch (Exception e){
            throw new WrongDateTimePatternException("Wrong pattern: "+pattern);
        }
    }
}
