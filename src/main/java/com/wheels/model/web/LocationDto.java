package com.wheels.model.web;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class LocationDto {

    @Size(min = 3, max = 10, message = "The Number not valid")
    private String carNumber;
    @Size(min = 3, max = 30, message = "The City not valid")
    private String city;
    @Size(min = 1, max = 30, message = "The Street not valid")
    private String street;
    @Positive(message = "Number of the house must be positive")
    private String house;
}
