package com.wheels.model.web;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserDto {

    private String email;
    private String password;


    private String firstName;
    private String lastName;
    private String phoneNumber;

    private String notes;
    private String avatar;
}
