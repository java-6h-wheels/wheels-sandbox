package com.wheels.model.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = "photoId")

@Entity
@Table(name = "photos")
public class Photo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int photoId;

    private String photoUrl;
    private String photoUrlPreview;
    private boolean isMainPhoto;

    @ManyToOne
    private Car car;

    public Photo(Car car, String photoUrl) {
        this.car = car;
        this.photoUrl = photoUrl;
    }
}
