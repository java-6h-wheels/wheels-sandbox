package com.wheels.model.entity;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = "cityName")

@Entity
@Table(name = "cities")
public class City {
    @Id
    @Column(length = 100)
    private String cityName;

    @OneToMany(mappedBy = "city")
    private Set<Location> locations = new HashSet<>();

    public City(String name) {
        this.cityName = name;
    }
}
