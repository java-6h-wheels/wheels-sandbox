package com.wheels.model.entity;

/*MINICOMPACT,
    SUBCOMPACT,
    COMPACT,
    MIDSIZE,
    FULLSIZE,
    FULLSIZELUXURY*/

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = "categoryName")

@Entity
@Table(name = "categories")
public class Category {

    @Id
    @Column(length = 100)
    private String categoryName;

    @JsonIgnore
    @OneToMany(mappedBy = "category", cascade = CascadeType.ALL)
    private Set<Car> cars = new HashSet<>();

    public Category(String name) {
        this.categoryName = name;
    }
}
