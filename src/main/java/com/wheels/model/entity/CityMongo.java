package com.wheels.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data

@Document(collection = CityMongo.COLLECTION_NAME)
public class CityMongo implements Serializable {
    public static final String COLLECTION_NAME = "cities";
    @Id
    private Long id;
    private String name;

}
