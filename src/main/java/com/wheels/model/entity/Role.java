package com.wheels.model.entity;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Getter @Setter
@EqualsAndHashCode(of = "role")

@Entity
@Table(name = "roles")
public class Role {
    @Id
    @Column(length = 100)
    private String role;

    @OneToMany(mappedBy = "role", cascade = CascadeType.ALL)
    private Set<User> users = new HashSet<>();

    public Role(String role){
        this.role = role;
    }
}
