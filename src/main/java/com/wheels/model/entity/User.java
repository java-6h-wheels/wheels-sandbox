package com.wheels.model.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = "email")

@Entity
@Table(name = "users")
public class User {

    @Id
    @Column(length = 100)
    private String email;

    private String password;
    private String firstName;
    private String lastName;
    private String phoneNumber;

    private LocalDate registerDate;
    private String notes;
    private String avatar;
    private boolean status;

    @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL)
    private Set<Car> cars = new HashSet<>();

    @ManyToOne
    private Role role;

    public User(String email, String password, String firstName, String lastName, String phoneNumber,
                String avatar, String notes) {
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.avatar = avatar;
        this.notes = notes;
        registerDate = LocalDate.now();
    }
}
