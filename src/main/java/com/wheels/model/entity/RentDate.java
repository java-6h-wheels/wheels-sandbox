package com.wheels.model.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = "rentDateId")

@Entity
@Table(name = "rent_dates")
public class RentDate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int rentDateId;

    @ManyToOne
    private Car car;

    private LocalDate startRent;
    private LocalDate endRent;
    private int rentDays;

    public RentDate(Car car, LocalDate startRent, LocalDate endRent) {
        this.car = car;
        this.startRent = startRent;
        this.endRent = endRent;
        rentDays = endRent.getDayOfYear() - startRent.getDayOfYear();
    }
}
