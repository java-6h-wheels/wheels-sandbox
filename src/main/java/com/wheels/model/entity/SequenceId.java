package com.wheels.model.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * wrapper class
 * AUTO_INCREMENT Mongo ObjectId
 *
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Document(collection = SequenceId.COLLECTION_NAME)
public class SequenceId {
    public static final String COLLECTION_NAME = "sequences";
    @Id
    private String id;
    private Long sequence;


}
