package com.wheels.model.entity;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;


@NoArgsConstructor
@Getter @Setter
@EqualsAndHashCode(of = "locationId")

@Entity
@Table(name = "locations")
public class Location {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int locationId;

    @ManyToOne
    private City city;

    private String street;
    private String house;
    private String coordinates;

    @OneToOne(mappedBy = "location")
    private Car car;

    public Location(City city, String street, String house) {
        this.city = city;
        this.street = street;
        this.house = house;
    }
}
