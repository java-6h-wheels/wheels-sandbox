package com.wheels.model.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Getter @Setter
@EqualsAndHashCode(of = "carNumber")

@Entity
@Table(name = "cars")
public class Car {

    @Id
    @Column(length = 100)
    private String carNumber;

    private String model;
    private int yearIssue;
    private double engine;
    private String color;
    private boolean ac;
    private boolean autoTransmission;
    private double rentPrice;
    private String notes;
    private LocalDate regDate;

    @ManyToOne
    private Category category;

    @OneToMany(mappedBy = "car")
    private Set<Photo> photos = new HashSet<>();

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "location_id", referencedColumnName = "locationId")
    private Location location;

    @OneToMany(mappedBy = "car", cascade = CascadeType.ALL)
    private Set<Comment> comments = new HashSet<>();

    @ManyToOne
    private User owner;

    @OneToMany(mappedBy = "car", cascade = CascadeType.ALL)
    private Set<RentDate> rentDates = new HashSet<>();

    public Car(User owner, String carNumber, String model, int yearIssue, double engine, String color, boolean ac, boolean autoTransmission,
               double rentPrice, String notes, Category category, Location location){
        this.owner = owner;
        this.carNumber = carNumber;
        this.model = model;
        this.yearIssue = yearIssue;
        this.engine = engine;
        this.color = color;
        this.ac = ac;
        this.autoTransmission = autoTransmission;
        this.rentPrice = rentPrice;
        this.notes = notes;
        this.category = category;
        this.location = location;
        regDate = LocalDate.now();
    }
}
