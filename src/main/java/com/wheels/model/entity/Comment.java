package com.wheels.model.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = "commentId")

@Entity
@Table(name = "comments")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int commentId;

    private LocalDate commentDate;
    private String comment;

    @ManyToOne
    private Car car;

    public Comment(Car car, String comment) {
        this.car = car;
        commentDate = LocalDate.now();
        this.comment = comment;
    }
}
