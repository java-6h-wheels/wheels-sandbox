package com.wheels.services;

import com.wheels.exceptions.SequenceException;
import com.wheels.model.entity.CityMongo;
import com.wheels.model.web.CityDto;
import com.wheels.model.web.DtoService;
import com.wheels.repository.CityMongoRepository;
import com.wheels.repository.SequenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;


@Service
public class CityMongoServiceImp implements CityMongoService {
    private static final String HOSTING_SEQ_KEY = "cities";
    @Autowired
    DtoService dtoService;

    @Autowired
    private SequenceRepository sequenceRepo;

    @Autowired
    private CityMongoRepository cityRepo;

    @Override
    public CityDto save(String name) throws SequenceException {

        CityMongo city = new CityMongo();

        city.setId(sequenceRepo.getNextSequenceId(HOSTING_SEQ_KEY));
        city.setName(name);
        cityRepo.save(city);


        return dtoService.getCityDtoMongo(city);
    }

    @Override
    public List<CityDto> getAll() {
        return dtoService.collectionToList(cityRepo.findAll(), dtoService.cityDtoMongo);
    }

    @Override
    public void remove(Long id) {
        cityRepo.deleteById(id);
    }

}
