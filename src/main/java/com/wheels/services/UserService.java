package com.wheels.services;


import com.wheels.model.web.UserDto;

import java.util.List;

public interface UserService {
    UserDto addUser(UserDto user);
    List<UserDto> getAllUsers();
    UserDto getUserByEmail(String email);
}
