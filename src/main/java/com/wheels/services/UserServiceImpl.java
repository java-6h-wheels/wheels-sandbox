package com.wheels.services;

import com.wheels.model.entity.User;
import com.wheels.model.web.DtoService;
import com.wheels.model.web.UserDto;
import com.wheels.repository.UserJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService{
    @Autowired
    UserJpaRepository userRepo;
    @Autowired
    DtoService dtoService;

    @Override
    public UserDto addUser(UserDto user) {
        User userEntity = new User(user.getEmail(), user.getPassword(), user.getFirstName(), user.getLastName(),
                user.getPhoneNumber(), user.getAvatar(), user.getNotes());
        userRepo.save(userEntity);
        return user;
    }

    @Override
    public List<UserDto> getAllUsers() {
        return dtoService.collectionToList(userRepo.findAll(), dtoService.userDto);
    }

    @Override
    public UserDto getUserByEmail(String email) {
        User user = userRepo.checkById(email, true);
        return dtoService.getUserDto(user);
    }
}
