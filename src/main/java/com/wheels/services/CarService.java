package com.wheels.services;

import com.wheels.model.web.*;

import java.time.LocalDate;
import java.util.List;

public interface CarService {
    CarDto addCar(CarDto car);
    CityDto addCity(CityDto city);
    CategoryDto addCategory(String category);
    RentDateDto addRentDate(RentDateDto rentDateDto);
    LocationDto addLocation(String carNumber, String city, String street, String house);
    CommentDto sendComment(CommentDto comment);
    CarDto getCarByNumber(String carNumber);
    List<CarDto> getAllCars();
    List<CategoryDto> getAllCategories();
    List<CityDto> getAllCities();
    List<CarPreviewDto> getCarsByFilter(Filter filter);
    List<String> getCarsNumberByRentDate(LocalDate startDate, LocalDate endDate);
    List<RentDateDto> getRentDatesByCarNumber(String carNumber);

    List<CarPreviewDto> getAllCarsPreview();
}
