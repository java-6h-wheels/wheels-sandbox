package com.wheels.services;

import com.wheels.model.web.CityDto;

import java.util.List;

public interface CityMongoService {

    CityDto save(String name);

    List<CityDto> getAll();

    void remove(Long id);
}
