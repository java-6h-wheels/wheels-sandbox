package com.wheels.services;

import com.wheels.common.DuplicatedIdException;
import com.wheels.exceptions.RentDateException;
import com.wheels.model.entity.*;
import com.wheels.model.web.*;
import com.wheels.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarServiceImpl implements CarService{
    @Autowired
    CarJpaRepository carRepo;
    @Autowired
    CityJpaRepository cityRepo;
    @Autowired
    CategoryJpaRepository categoryRepo;
    @Autowired
    UserJpaRepository userRepo;
    @Autowired
    DtoService dtoService;
    @Autowired
    RentDateJpaRepository rentDateRepo;
    @Autowired LocationJpaRepository locRepo;
    @Autowired PhotoJpaRepository photoRepo;
    @Autowired CommentJpaRepository commentRepo;

    @Override
    public CarDto addCar(CarDto car) {
        carRepo.checkById(car.getCarNumber(), false);
        Category category = categoryRepo.checkById(car.getCategory(), true);
        User owner = userRepo.checkById(car.getOwnerEmail(), true);
        Location location = new Location(cityRepo.checkById(car.getCity(), true), car.getStreet(), car.getHouse());
        locRepo.save(location);

        Car carEntity = new Car(owner, car.getCarNumber(), car.getModel(), car.getYearIssue(), car.getEngine(), car.getColor(), car.isAc(),
                car.isAutoTransmission(), car.getRentPrice(), car.getNotes(), category, location);
        for (int i = 0; i< car.getPhotoUrl().length; i++){
            Photo photo = new Photo(carEntity, car.getPhotoUrl()[i]);
            if (i == 0){
                photo.setMainPhoto(true);
            }
            carEntity.getPhotos().add(photoRepo.save(photo));
        }
        category.getCars().add(carEntity);
        categoryRepo.save(category);
        owner.getCars().add(carEntity);
        location.setCar(carEntity);
        locRepo.save(location);
        userRepo.save(owner);
        carRepo.save(carEntity);
        return car;
    }

    @Override
    public CityDto addCity(CityDto city) {
        cityRepo.checkById(city.getName(), false);
        cityRepo.save(new City(city.getName()));
        return city;
    }

    @Override
    public CategoryDto addCategory(String category) {
        categoryRepo.checkById(category, false);
        categoryRepo.save(new Category(category));
        return new CategoryDto(category);
    }


    @Override
    public RentDateDto addRentDate(RentDateDto rentDate) {
        if (rentDateRepo.findCarNumberByRentalDates(LocalDate.parse(rentDate.getStartRent()), LocalDate.parse(rentDate.getEndRent())).contains(rentDate.getCarNumber()))
            throw new RentDateException("These dates are already booked!");
        Car car = carRepo.checkById(rentDate.getCarNumber(), true);
        RentDate rentDateEntity = new RentDate(car, LocalDate.parse(rentDate.getStartRent()), LocalDate.parse(rentDate.getEndRent()));
        car.getRentDates().add(rentDateEntity);
        rentDateRepo.save(rentDateEntity);
        carRepo.save(car);
        return rentDate;
    }

    @Override
    public LocationDto addLocation(String carNumber, String city, String street, String house) {
        Car car = carRepo.checkById(carNumber, true);
        City cityEntry = cityRepo.checkById(city, true);
        Location location = new Location(cityEntry, street, house);
        location.setCar(car);
        locRepo.save(location);
        return dtoService.getLocationDto(location);
    }

    @Override
    public CommentDto sendComment(CommentDto comment) {
        Car car = carRepo.checkById(comment.getCarNumber(), true);
        Comment commentEntity = new Comment(car, comment.getComment());
        commentRepo.save(commentEntity);
        return comment;
    }

    @Override
    @Transactional(readOnly = true)
    public CarDto getCarByNumber(String carNumber) {
        return dtoService.getCarDto(carRepo.checkById(carNumber, true));
    }

    @Override
    @Transactional(readOnly = true)
    public List<CarDto> getAllCars() {
        return dtoService.collectionToList(carRepo.findAll(), dtoService.carDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CategoryDto> getAllCategories() {
        return dtoService.collectionToList(categoryRepo.findAll(), dtoService.categoryDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CityDto> getAllCities() {
        return dtoService.collectionToList(cityRepo.findAll(), dtoService.cityDto);
    }

    @Override
    public List<CarPreviewDto> getCarsByFilter(Filter filter) {
        List<String> carsByDate;
        List<Car> carsByFilter = carRepo.findCarByFilter(filter);
        if (filter.getStartRent() != null && filter.getEndRent() != null) {
            carsByDate = rentDateRepo.findCarNumberByRentalDates(LocalDate.parse(filter.getStartRent()), LocalDate.parse(filter.getEndRent()));
            return dtoService.collectionToList(carsByFilter.stream().filter(car -> !carsByDate.contains(car.getCarNumber())).collect(Collectors.toList()), dtoService.carPreviewDto);
        }
        return dtoService.collectionToList(carsByFilter, dtoService.carPreviewDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<String> getCarsNumberByRentDate(LocalDate startDate, LocalDate endDate) {
        return rentDateRepo.findCarNumberByRentalDates(startDate, endDate);
    }

    @Override
    @Transactional(readOnly = true)
    public List<RentDateDto> getRentDatesByCarNumber(String carNumber) {
        Car car = carRepo.checkById(carNumber, true);
        return dtoService.collectionToList(rentDateRepo.findByCar(car), dtoService.rentDateDto);
    }

    @Override
    public List<CarPreviewDto> getAllCarsPreview() {
        return dtoService.collectionToList(carRepo.findAll(), dtoService.carPreviewDto);
    }
}
