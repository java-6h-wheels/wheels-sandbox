package com.wheels.security.repositories;

import com.wheels.security.entities.Account;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AccountMongoRepository extends MongoRepository<Account, String> {
}
