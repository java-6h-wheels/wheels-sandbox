package com.wheels.security.entities;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Getter @Setter
@Data

@Document(collection = "account")
public class Account {

    private String login;
    private String password;

    private Set<String> roles = new HashSet<>();

    public Account(String login, String password, String role) {
        this.login = login;
        this.password = password;
        this.roles.add(role);
    }
}
