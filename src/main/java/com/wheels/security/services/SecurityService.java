package com.wheels.security.services;

import com.wheels.security.dto.AccountDto;

public interface SecurityService {

    AccountDto addAccount(String login, String password, String role);
    AccountDto grantRole(String login, String role);
}
