package com.wheels.security.services;

import com.wheels.security.dto.AccountDto;
import com.wheels.security.entities.Account;
import com.wheels.security.repositories.AccountMongoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.server.ResponseStatusException;

public class SecurityServiceImpl implements SecurityService{
    @Autowired
    AccountMongoRepository accountRepo;
    @Autowired
    PasswordEncoder encoder;
    @Autowired DtoSecurityService dtoSecurityService;

    private Account getAccount(String login) {
        Account account = accountRepo.findById(login).orElse(null);
        if (account == null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Login "+login+" not found");
        return account;
    }
    @Override
    public AccountDto addAccount(String login, String password, String role) {
        if (accountRepo.existsById(login))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Duplicated login "+login);

        Account account = new Account(login, encoder.encode(password), "ROLE_"+role);
        accountRepo.save(account);
        return dtoSecurityService.accountDto(account);
    }

    @Override
    public AccountDto grantRole(String login, String role) {
        Account account = getAccount(login);
        account.getRoles().add("ROLE_"+role);
        accountRepo.save(account);

        return dtoSecurityService.accountDto(account);
    }
}
