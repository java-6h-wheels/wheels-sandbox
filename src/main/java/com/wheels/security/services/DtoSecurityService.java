package com.wheels.security.services;

import com.wheels.security.dto.AccountDto;
import com.wheels.security.entities.Account;

import java.util.List;
import java.util.stream.Collectors;

public class DtoSecurityService {

    public AccountDto accountDto(Account account){
        return new AccountDto(account.getLogin(), account.getRoles());
    }
    public List<AccountDto> accountDtoList(List<Account> accountList) {
        return accountList.stream().map(this::accountDto).collect(Collectors.toList());
    }
}
