package com.wheels.repository;

import com.wheels.common.CustomizedJpaRepository;
import com.wheels.model.entity.Car;
import com.wheels.model.entity.RentDate;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;

public interface RentDateJpaRepository extends CustomizedJpaRepository<RentDate, Integer> {

    @Query("SELECT DISTINCT(rd.car.carNumber) FROM RentDate rd WHERE " +
            "(rd.startRent BETWEEN ?1 AND ?2) OR (rd.endRent BETWEEN ?1 AND ?2) OR (rd.startRent <= ?1 AND rd.endRent >= ?2)")
    List<String> findCarNumberByRentalDates(LocalDate startRent, LocalDate endRent);
    List<RentDate> findByCar(Car car);
}
