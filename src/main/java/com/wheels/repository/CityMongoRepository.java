package com.wheels.repository;

import com.wheels.model.entity.CityMongo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CityMongoRepository extends MongoRepository<CityMongo, Long> {

}
