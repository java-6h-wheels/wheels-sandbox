package com.wheels.repository;

import com.wheels.common.CustomizedJpaRepository;
import com.wheels.model.entity.Comment;

public interface CommentJpaRepository extends CustomizedJpaRepository<Comment, Integer> {
}
