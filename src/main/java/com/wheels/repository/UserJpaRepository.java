package com.wheels.repository;

import com.wheels.common.CustomizedJpaRepository;
import com.wheels.model.entity.User;

public interface UserJpaRepository extends CustomizedJpaRepository<User, String> {
    @Override
    default String type() {
        return "User";
    }
    User findByEmail(String email);
}
