package com.wheels.repository;

import com.wheels.common.CustomizedJpaRepository;
import com.wheels.model.entity.Car;
import com.wheels.model.entity.City;
import com.wheels.model.entity.RentDate;
import com.wheels.model.web.Filter;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

interface CustomizedCarJpaRepository{
    List<Car> findCarByFilter(Filter filter);
}
class CustomizedCarJpaRepositoryImpl implements CustomizedCarJpaRepository{

    @PersistenceContext
    EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<Car> findCarByFilter(Filter filter) {
        StringBuilder strQuery = new StringBuilder();
        strQuery.append("SELECT car FROM Car car WHERE ");
        if (filter.getCarNumber().length() > 0) strQuery.append("car.carNumber='").append(filter.getCarNumber()).append("' AND ");
        if (filter.getCategory().length() > 0) strQuery.append("car.category.categoryName='").append(filter.getCategory()).append("' AND ");
        if (filter.getModel().length() > 0) strQuery.append("car.model='").append(filter.getModel()).append("' AND ");
        if (filter.getStartYearIssue() > 0 && filter.getEndYearIssue() > 0)
            strQuery.append("(car.yearIssue BETWEEN ")
                    .append(filter.getStartYearIssue()).append(" AND ")
                    .append(filter.getEndYearIssue()).append(") AND ");
        if (filter.getEngine() > 0) strQuery.append("car.engine=").append(filter.getEngine()).append(" AND ");
        if (filter.getColor().length() > 0) strQuery.append("car.color='").append(filter.getColor()).append("' AND ");
        strQuery.append("car.ac=").append(filter.isAc()).append(" AND ");
        strQuery.append("car.autoTransmission=").append(filter.isAutoTransmission()).append(" AND ");
        if (filter.getCity().length() > 0) strQuery.append("car.location.city.cityName='").append(filter.getCity()).append("' AND ");
        if (filter.getStartRentPrice() > 0 && filter.getEndRentPrice() > 0)
            strQuery.append("(car.rentPrice BETWEEN ")
                    .append(filter.getStartRentPrice()).append(" AND ")
                    .append(filter.getEndRentPrice()).append(") AND ");
        String resultStr = strQuery.substring(0, strQuery.length()-4);
        //System.out.println(resultStr.concat(";"));
        return entityManager.createQuery(resultStr).getResultList();
    }
}
public interface CarJpaRepository extends CustomizedJpaRepository<Car, String>, CustomizedCarJpaRepository {
    @Override
    default String type() {
        return "Car";
    }

}
