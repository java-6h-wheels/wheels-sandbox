package com.wheels.repository;

import com.wheels.common.CustomizedJpaRepository;
import com.wheels.model.entity.Category;

public interface CategoryJpaRepository extends CustomizedJpaRepository<Category, String> {
    @Override
    default String type() {
        return "Category";
    }
}
