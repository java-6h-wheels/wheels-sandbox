package com.wheels.repository;

import com.wheels.exceptions.SequenceException;

public interface SequenceRepository {
    long getNextSequenceId(String key) throws SequenceException;
}
