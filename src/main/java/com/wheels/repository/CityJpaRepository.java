package com.wheels.repository;

import com.wheels.common.CustomizedJpaRepository;
import com.wheels.model.entity.City;

public interface CityJpaRepository extends CustomizedJpaRepository<City, String> {
    @Override
    default String type() {
        return "City";
    }
}
