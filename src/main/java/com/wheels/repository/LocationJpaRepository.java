package com.wheels.repository;

import com.wheels.common.CustomizedJpaRepository;
import com.wheels.model.entity.Location;

public interface LocationJpaRepository extends CustomizedJpaRepository<Location, Integer> {
}
