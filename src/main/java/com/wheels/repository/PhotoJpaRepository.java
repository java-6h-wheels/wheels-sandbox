package com.wheels.repository;

import com.wheels.common.CustomizedJpaRepository;
import com.wheels.model.entity.Photo;

public interface PhotoJpaRepository extends CustomizedJpaRepository<Photo, String> {
    @Override
    default String type() {
        return "Photo";
    }
}
