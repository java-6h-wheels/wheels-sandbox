package com.wheels.randoms;

import java.time.LocalDate;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class RandomLib {

		public static Random gen = new Random();

		public static int nextIntRandomInRange (int min, int max){
			return min + gen.nextInt(max - min+1);
		}
		public static int nextIntRandom (int max){
			return gen.nextInt(max);
		}
		public static Long nextLongRandomInRange (long min, long max){
			return (long) nextRandomDoubleInRange(min, max);
		}

		public static String generatePassword (){
			return 
				Long.toString(
					Long.parseLong(
							Double.toString(gen.nextDouble()).substring(2)),36);
		}
		
		public static LocalDate randomLocalDate(LocalDate from, LocalDate to) {
			long epochDaysFrom = from.toEpochDay();
			long epochDaysTo = to.toEpochDay();
			return LocalDate.ofEpochDay(ThreadLocalRandom.current()
				.nextLong(epochDaysFrom, epochDaysTo+1));
		}
		
		public static double nextRandomDoubleInRange (double min, double max){
			return min + gen.nextDouble()*(max-min);
		}
		
		
		public static String getLettersAndDigitsInLength (int maxLenght){
			
			char[] array = new char[maxLenght];
			int randomChar;
			int i = 0;
			while (i < maxLenght) {
				randomChar =  nextIntRandomInRange(48, 90); //alfabet + digits
			    if(randomChar < 58 || randomChar > 64) {
			    	array[i] = (char) randomChar;
			    	i++;
			    }
			}
			String result = new String(array);
			return result;
		}
		public static boolean nextBoolean (){
			return gen.nextBoolean();
		}
		public static boolean nextBooleanWithProbability (double trueProbability){
			return gen.nextDouble() < trueProbability;
		}
		public static String nextStringFromArray (String[] arr){
			return arr[gen.nextInt(arr.length)];
		}
}		
