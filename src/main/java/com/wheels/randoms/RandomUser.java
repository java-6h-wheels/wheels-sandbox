package com.wheels.randoms;


import com.wheels.model.web.UserDto;
import lombok.Data;

import static com.wheels.randoms.RandomLib.*;

@Data
public class RandomUser {
    private static final String[] NAMES = {"James", "Jacob", "Michael", "Ethan", "Tyler", "Aiden", "Joshua",
            "Joseph", "Noah", "Matthew", "Emma", "Ava", "Abigail", "Sophia",
            "Emily", "Isabella", "Olivia", "Elizabeth", "Madison", "Alyssa"};
    private static final String[] LAST_NAMES = {"Smith", "Johnson", "Williams", "Brown", "Jones", "Miller", "Davis",
            "Garcia", "Rodriguez", "Wilson", "Martinez", "Anderson", "Taylor",
            "Thomas", "Hernandez", "Moore", "Martin", "Jackson", "Thompson", "White"};
    private static int EMAIL_MAX_DIGITS = 4;


    public static UserDto of(){
        String firstName = nextStringFromArray(NAMES);
        String lastName = nextStringFromArray(LAST_NAMES);

        String email = firstName + lastName +
                nextIntRandom(EMAIL_MAX_DIGITS) + "@gmail.com";
        String password = generatePassword();


        String phoneNumber = nextLongRandomInRange(100000000, 999999999).toString();

        return new UserDto(email, password, firstName, lastName, phoneNumber, "","");
    }
}
