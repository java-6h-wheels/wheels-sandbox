package com.wheels.randoms;

import com.wheels.model.web.CarDto;

import java.time.LocalDate;

import static com.wheels.randoms.RandomLib.*;


public class RandomCar {
    private static final String[] MODELS = {"Audi", "BMW", "Ford",
            "Honda", "Kia", "Lexus",
            "Mazda", "Nissan", "Skoda"};
    private static final String[] CATEGORIES = {"MINICOMPACT", "SUBCOMPACT", "COMPACT",
            "MIDSIZE", "FULLSIZE", "FULLSIZELUXURY"};
    private static final String[] COLORS = {"blue", "white", "red",
            "green", "black", "silver"};
    private static final String[] CITIES = {"Acre", "Ashdod", "Ashkelon", "Bat Yam",
            "Beersheba", "Bnei Brak", "Eilat", "Haifa",
            "Jerusalem", "Lod", "Nazareth", "Netanya",
            "Ramat Gan", "Tel Aviv-Yafo", "Safed"};
    private static final int CAR_NUMBER_LENGTH = 9;
    private static final int MIN_YEAR = 2000;
    private static final double MIN_ENGINE = 2.;
    private static final double MAX_ENGINE = 8.;
    private static final double MIN_PRICE = 10.;
    private static final double MAX_PRICE = 100.;

    public static CarDto of(){
        String carNumber = getLettersAndDigitsInLength(CAR_NUMBER_LENGTH);
        String ownerEmail = "";//TODO
        String category = nextStringFromArray(CATEGORIES);
        String model = nextStringFromArray(MODELS);
        int yearIssue = nextIntRandomInRange(MIN_YEAR, LocalDate.now().getYear());
        double engine = nextRandomDoubleInRange(MIN_ENGINE, MAX_ENGINE);
            engine = ((int)(engine*10))/10.;
        String color = nextStringFromArray(COLORS);
        boolean ac = nextBooleanWithProbability(0.7);
        boolean autoTransmission = nextBoolean();
        double rentPrice = nextRandomDoubleInRange(MIN_PRICE, MAX_PRICE);
        rentPrice = ((int)(rentPrice*10))/10.;
        String notes = "";
        String[] photoUrl = new String[10];
        String[] comments = new String[10];

        String city = nextStringFromArray(CITIES);;
        String street = "SomeStreet";
        String house = "1B";
        return new CarDto(carNumber, ownerEmail, category, model, yearIssue, engine,
                color, ac, autoTransmission, rentPrice, notes, photoUrl, comments, city, street, house);
    }
}
