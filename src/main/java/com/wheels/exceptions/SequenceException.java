package com.wheels.exceptions;

public class SequenceException extends RuntimeException {
    public SequenceException(String message) {
        super(message);
    }
}
