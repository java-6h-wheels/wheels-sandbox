package com.wheels.exceptions;

public class RentDateException extends RuntimeException{

    public RentDateException(String msg) {
        super(msg);
    }
}
