package com.wheels.exceptions;

public class WrongDateTimePatternException extends RuntimeException {
    public WrongDateTimePatternException(String msg) {
        super(msg);
    }
}
