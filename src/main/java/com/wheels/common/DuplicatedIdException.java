package com.wheels.common;

public class DuplicatedIdException extends RuntimeException {
    public DuplicatedIdException(String msg) {
        super(msg);
    }
}
