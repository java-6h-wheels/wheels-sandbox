package com.wheels.controllers;

import com.wheels.common.DuplicatedIdException;
import com.wheels.common.IdNotFoundException;
import com.wheels.exceptions.RentDateException;
import com.wheels.exceptions.WrongDateTimePatternException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class ExceptionHandlers {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<List<String>> handleValidationsErrors(MethodArgumentNotValidException exception){

        BindingResult bindingResult = exception.getBindingResult();
        ArrayList<String> result = bindingResult.getAllErrors().stream().map(e -> {
            String rejectedValue = "";
            try {
                rejectedValue = ": " + ((FieldError) e).getRejectedValue();
            } catch (Exception ignored) {
            }
            return e.getDefaultMessage() + rejectedValue;
        }).collect(Collectors.toCollection(ArrayList::new));
        return new ResponseEntity<>(result,HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<List<String>> handleConstraintErrors(ConstraintViolationException exception){
        List<String> result = exception.getConstraintViolations().stream()
                .map(e -> e.getMessage() + ": " + e.getInvalidValue()).collect(Collectors.toList());
        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<String> handleJsonErrors(HttpMessageNotReadableException exception){
        return new ResponseEntity<>("User request error! " + exception.getMessage(), HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handleOtherErrors(Exception exception){
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(value = {DuplicatedIdException.class,
            IdNotFoundException.class,
            RentDateException.class,
            WrongDateTimePatternException.class})
    public ResponseEntity<?> handleMyErrors(RuntimeException exception){
        exception.printStackTrace();
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }

}
