package com.wheels.controllers;

import com.wheels.model.entity.Car;
import com.wheels.model.entity.Location;
import com.wheels.model.entity.RentDate;
import com.wheels.model.web.*;
import com.wheels.services.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
@Valid
public class UserController {

    @Autowired
    CarService carService;

    @GetMapping("/get_all_cities")
    public List<CityDto> getAllCities(){
        return carService.getAllCities();
    }

    @GetMapping("/get_all_categories")
    public List<CategoryDto> getAllCategories(){
        return carService.getAllCategories();
    }

    @PostMapping("/get_cars_by_filter")
    public List<CarPreviewDto> getCarsByFilter(@RequestBody Filter filter){
        System.out.println(filter);
        return carService.getCarsByFilter(filter);
    }
    //all requests by supplier

    @PostMapping("/supplier/add_car")
    public CarDto addCar(@RequestBody @Valid CarDto car){
        return carService.addCar(car);
    }

    @DeleteMapping("/supplier/remove_car")
    public boolean removeCar(String number){
        return true;
    }

    @PostMapping("/supplier/change_rentPrice")
    public double changeRentPrice(String number, double price){
        return 0;
    }

    @PostMapping("/supplier/change_car")
    public CarDto changeCar(String oldCarNumber, @RequestBody CarDto changedCar){
        return new CarDto();
    }

    @PostMapping("/supplier/add_rental_date")
    public RentDateDto addRentalDate(@RequestBody RentDateDto rentDateDto){
        return carService.addRentDate(rentDateDto);
    }

    //all request by renter

    @GetMapping("/renter/get_cars_by_location")
    public List<CarDto> getCarsByLocation(@RequestBody LocationDto location, int searchRadius){
        return new ArrayList<>();
    }

    @PostMapping("/renter/send_comment")
    public CommentDto sendComment(@RequestBody CommentDto comment){
        return carService.sendComment(comment);
    }

    @GetMapping("/renter/get_cars_by_rentalDate")
    public List<String> getCarsByRentalDate(
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startRent,
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endRent){
        return carService.getCarsNumberByRentDate(startRent, endRent);
    }
    @GetMapping("/renter/get_car_by_number")
    public CarDto getCarByNumber(String carNumber){
        return carService.getCarByNumber(carNumber);
    }
}
