package com.wheels.controllers;

import com.wheels.model.entity.Car;
import com.wheels.model.entity.User;
import com.wheels.model.web.*;
import com.wheels.repository.UserJpaRepository;
import com.wheels.services.CarService;
import com.wheels.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/admin")
@Valid
public class AdminController {

    @Autowired
    UserService userService;
    @Autowired
    CarService carService;

    @PostMapping("/add_city")
    public CityDto addCity(@RequestBody CityDto city){
        return carService.addCity(city);
    }
    @PostMapping("/add_category")
    public CategoryDto addCategory(String category){
        return carService.addCategory(category);
    }

    //all requests by car
    @GetMapping("/get_all_cars")
    public List<CarDto> getAllCars(){
        return carService.getAllCars();
    }
    @GetMapping("/get_all_car_preview")
    public List<CarPreviewDto> getAllCarsPreview(){
        return carService.getAllCarsPreview();
    }
    @GetMapping("/car/get_all_cars_with_empty_fields")
    public List<CarDto> getAllCarsWithEmptyFields(){
        return new ArrayList<>();
    }
    @GetMapping("/car/get_all_cars_by_regDate_range")
    public List<CarDto> getAllCarsByRegDateRange(
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromRegDate,
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate toRegDate){
        return new ArrayList<>();
    }
    @GetMapping("/car/get_all_cars_by_user")
    public List<CarDto> getAllCarsByUsers(String email){
        return new ArrayList<>();
    }
    @GetMapping("/get_most_popular_cars")
    public List<CarDto> getMostPopularCars(){
        return new ArrayList<>();
    }
    @GetMapping("/get_rentDate_by_car")
    public List<RentDateDto> getRentDateByCar(String carNumber){
        return carService.getRentDatesByCarNumber(carNumber);
    }

    //all requests by user

    @PostMapping("/user/create_user")
    public UserDto createUser(@RequestBody UserDto user){
        return userService.addUser(user);
    }
    @GetMapping("/user/get_users_by_registerDate_range")
    public List<UserDto> getUsersByRegisterDateRange(
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromRegDate,
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate toRegDate){
        return new ArrayList<>();
    }
    @GetMapping("/user/get_all_users_without_car")
    public List<UserDto> getAllUsersWithoutCar(){
        return new ArrayList<>();
    }
    @GetMapping("/user/get_all_users_with_empty_fields")
    public List<UserDto> getAllUsersWithEmptyFields(){
        return new ArrayList<>();
    }
    @GetMapping("/user/get_all_users_with_car")
    public List<UserDto> getAllUserWithCar(){
        return new ArrayList<>();
    }
    @GetMapping("/user/get_all_users")
    public List<UserDto> getAllUsers(){
        return userService.getAllUsers();
    }
    @DeleteMapping("/user/remove_user")
    public UserDto removeUser(int userId){
        return new UserDto();
    }
    @PostMapping("/user/block_user")
    public UserDto blockUser(int userId){
        return new UserDto();
    }
    @PostMapping("/user/unblock_user")
    public UserDto unblockUser(int userId){
        return new UserDto();
    }
    @PostMapping("/user/update_user")
    public UserDto updateUser(int oldUserId, UserDto newUser){
        return new UserDto();
    }
    @GetMapping("/user/get_user_by_email")
    public UserDto getUserByEmail(String email){
        return new UserDto();
    }
}
