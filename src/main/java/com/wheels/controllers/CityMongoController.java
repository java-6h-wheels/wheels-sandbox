package com.wheels.controllers;


import com.wheels.model.web.*;
import com.wheels.services.CityMongoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/mongo_cities")
@Valid
public class CityMongoController {

    @Autowired
    CityMongoService cityMongoService;

    @GetMapping("/all")
    public List<CityDto> getAllCities(){
        return cityMongoService.getAll();
    }

    @PostMapping("/add")
    public CityDto addCity(String cityName){
        return cityMongoService.save(cityName);
    }
/*
    @PostMapping("/update")
    public void addCity(Long id, String cityName){
        mongoCityService.update(id, cityName);
    }
*/

    @DeleteMapping("/remove")
    public boolean removeCity(Long id){
        cityMongoService.remove(id);
        return true;
    }


}

