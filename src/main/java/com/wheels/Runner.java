package com.wheels;

import com.wheels.model.entity.*;
import com.wheels.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class Runner implements CommandLineRunner {

    @Autowired private CarJpaRepository carRepo;
    @Autowired private CategoryJpaRepository categoryRepo;
    @Autowired private UserJpaRepository userRepo;
    @Autowired private CityJpaRepository cityRepo;
    @Autowired private LocationJpaRepository locRepo;

    private User user1 = new User("bond@gmail.com", "111", "Den", "Bond", "321654",
                     "", "");
    private User user2 = new User("qwer@gmail.com", "222", "Pol", "Kort", "321654",
                                 "", "");

    private Category cat1 = new Category("FULLSIZE");
    private Category cat2 = new Category("MIDSIZE");
    @Override
    public void run(String... args){

        categoryRepo.saveAll(Stream.of(
                cat1,
                cat2)
                .collect(Collectors.toList()));
        userRepo.saveAll(Stream.of(
                user1,
                user2)
                .collect(Collectors.toList()));
        cityRepo.saveAll(Stream.of(
                new City("Tel Aviv"),
                new City("Haifa"),
                new City("Ashdod"))
                .collect(Collectors.toList()));
        carRepo.saveAll(Stream.of(
                new Car(user1, "A111AA", "BMW", 2015, 2.5, "Black", true, true,
        500., "notes", cat2, locRepo.save(new Location(cityRepo.checkById("Tel Aviv", true), "Vaiczman", "5"))),
                new Car(user2, "A222AA", "AUDI", 2018, 3.0, "Black", true, true,
                        800., "notes", cat2,
                        locRepo.save(new Location(cityRepo.checkById("Haifa", true), "Histadrut", "12"))),
                new Car(user1, "A333AA", "KIA", 2018, 1.5, "Red", true, true,
                        250., "notes", cat2, locRepo.save(new Location(cityRepo.checkById("Ashdod", true), "Vaiczman", "5"))),
                new Car(user2, "A444AA", "RENO", 2012, 1.8, "Red", true, true,
                        250., "notes", cat2, locRepo.save(new Location(cityRepo.checkById("Haifa", true), "Vaiczman", "5"))))
                .collect(Collectors.toList()));
    }
}
